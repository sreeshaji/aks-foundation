locals{
  confirm_rg = (var.resource_group_name == null && var.environment == "pre-prod") ? "blpocacrpreprd-rg" : var.resource_group_name
  rg_tags = {
    app        = "test"
    "env"      = "pre-prod"
    "location" = "azure"
    "region"   = "singapore"
    "service"  =  "acr"
    "squad"    = "cloud"
    "tribe"    = "infra"
  }
}
data "external" "check_rg" {
    program = ["/bin/bash","./script.sh"]

    query = {
        group_name = local.confirm_rg
    }
}

resource "azurerm_resource_group" "resource_group" {
  count             = data.external.check_rg.result.exists == "true" ? 0 : 1
  name              = local.confirm_rg
  location          = coalesce(var.location,"Southeast Asia")
  tags              = local.rg_tags
}

resource "azurerm_container_registry" "acr" {
  name                = (var.acr_name == null && var.environment == "pre-prod") ? "blpocacrpreprd" : var.acr_name
  resource_group_name = local.confirm_rg
  location            = var.environment == "pre-prod" ? coalesce(var.location,"Southeast Asia") : var.location
  sku                 = var.environment == "pre-prod" ? coalesce(var.sku,"Premium") : var.sku
  tags                = var.additional_tags != null ? var.additional_tags : null
}
