variable "acr_name"{
    type = string
    description = "Required for prod environment,optional for sandbox, where the acr will be created with a default name"
    default = null
}

variable "location"{
    type    = string
    description = "Required for prod environment,optional for sandbox, where ACR will be deployed to Suutheast asia"
    default = null
}

variable "resource_group_name"{
    type    = string
    description = "Required for prod environment,optional for sandbox, where ACR will be deployed to a default resource group"
    default = null
}

variable "sku"{
    type    = string
    description = "Required for prod environment,optional for sandbox, where sku will be taken as premium"
    default = null
}

variable "environment"{
    type = string
    description = "Required for all environments"
    default ="sandbox"
}
variable "additional_tags"{
    type = map(string)
    description = "(Optional) for environments"
    default = null
}